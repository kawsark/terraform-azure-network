provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "example" {
  name     = "kawsar-az-network"
  location = "West US"
}

module "network" {
 source  = "app.terraform.io/GitlabCI-demos/network/azurerm"
  version = "3.5.0"
 
  resource_group_name = azurerm_resource_group.example.name


  address_spaces      = ["10.0.0.0/16", "10.2.0.0/16"]
  subnet_prefixes     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  subnet_names        = ["subnet1", "subnet2", "subnet4"]

  subnet_service_endpoints = {
    "subnet1" : ["Microsoft.Sql"], 
    "subnet2" : ["Microsoft.Sql"],
    "subnet3" : ["Microsoft.Sql"]
  }

  tags = {
      owner = "kawsar-dev"
      ttl = "2h"
    environment = "aisdemo"
    costcenter  = "it"
  }

  depends_on = [azurerm_resource_group.example]
}
